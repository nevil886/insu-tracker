export const navigation = [
    {
        name: 'Dashboard',
        url: '/dashboard',
        icon: 'icon-speedometer'
    },
    {
      title: true,
      name: 'Admin'
    },
    {
      name: 'Customers',
      url: '/admin-mgt/customer',
      icon: 'fas fa-users'
    },
    // {
    //   name: 'Discount Summary',
    //   url: '/admin-mgt/discount-summary',
    //   icon: 'fas fa-percent'
    // },
    {
      menu: true,
      entitlements: [],
      name: 'Configurations',
      url: '/admin-mgt/config',
      icon: 'fas fa-cogs',
      children: [
        {
          entitlements: [],
          name: 'Accident Spots',
          url: '/admin-mgt/config/accident-spots',
          icon: 'fas fa-align-justify'
        }
      ]
    },
    {
        title: true,
        name: 'Reports'
    },
    {
        divider: true
    }
];
