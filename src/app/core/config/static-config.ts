export class StaticConfig {

  public static APP_NAME = 'INSU TRACKER';

  public static STATUS_LIST = {
    'CREATED': {
      ID: 0,
      NAME: 'Create',
      COLOR: '#000000'
    },
    'PENDING': {
      ID: 1,
      NAME: 'Pending',
      COLOR: '#0095ff'
    },
    'APPROVED': {
      ID: 2,
      NAME: 'Approved',
      COLOR: '#11c14b'
    },
    'CANCELED': {
      ID: 3,
      NAME: 'Cancel',
      COLOR: '#8B4513'
    },
    'REVERTED': {
      ID: 4,
      NAME: 'Reverted',
      COLOR: '#000000'
    },
    'REJECTED': {
      ID: 5,
      NAME: 'Rejected',
      COLOR: '#dc3545'
    },
    'SUSPENDED': {
      ID: 6,
      NAME: 'Hold',
      COLOR: '#f5a718'
    },
    'BLACKLISTED': {
      ID: 7,
      NAME: 'Blacklisted',
      COLOR: '#000000'
    },
    'DELETED': {
      ID: 8,
      NAME: 'Deleted',
      COLOR: '#ea1212'
    },
    'AMENDED': {
      ID: 9,
      NAME: 'Amended',
      COLOR: '#000000'
    },
    'ACCEPTED': {
      ID: 10,
      NAME: 'Accepted',
      COLOR: '#11c110'
    },
    'RELEASED': {
      ID: 11,
      NAME: 'Prepared',
      COLOR: '#000000'
    },
    'DISPATCHED': {
      ID: 12,
      NAME: 'Dispatched',
      COLOR: '#000000'
    },
    'DELIVERED': {
      ID: 13,
      NAME: 'Delivered',
      COLOR: '#000000'
    },
    'DELIVERY_ACCEPTED': {
      ID: 14,
      NAME: 'Delivery Accepted',
      COLOR: '#000000'
    },
    'DELIVERY_REJECTED': {
      ID: 15,
      NAME: 'Delivery Rejected',
      COLOR: '#000000'
    }
  };

}
