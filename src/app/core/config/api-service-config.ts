export class ApiServiceConfig {

  public static ADMIN_API_SERVICE = {
    NAME : 'Admin Management Service',
    KEY : 'GBL_SEV',
    ROUTE_PATH : '/admin_service'
  };

}
