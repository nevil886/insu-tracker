import {Inject, Injectable} from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {LOCAL_STORAGE, WebStorageService} from 'angular-webstorage-service';

import {GlobalVariable} from '../com-classes';

@Injectable()
export class GuardService implements CanActivate {
  constructor(private router: Router,
              @Inject(LOCAL_STORAGE) private storage: WebStorageService,
              private gVariable: GlobalVariable) {
  }

  public canActivate() {
    try {
      const authentication = this.storage.get('auth_insu_track');

      if (authentication !== null && authentication.authorized) {
        this.gVariable.authentication = authentication;
        return true;
      } else {
        this.removeAuthentication();
        return false;
      }
    } catch (e) {
      this.removeAuthentication();
      return false;
    }
  }

  public createAuthentication(value: any) {
    value.authorized = true;
    this.storage.set('auth_insu_track', value);
    this.canActivate();
  }

  public removeAuthentication() {
    this.storage.remove('auth_insu_track');
    this.gVariable.authentication = {};
    this.router.navigate(['/login']);
  }

}
