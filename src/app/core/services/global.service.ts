import {Inject, Injectable} from '@angular/core';
import {LOCAL_STORAGE, WebStorageService} from 'angular-webstorage-service';
import {navigation} from '../config';
import {GlobalVariable} from '../com-classes';
import {GuardService} from './guard.service';

@Injectable()
export class GlobalService {

  private navigation: any = navigation || [];

  constructor(private gVariable: GlobalVariable,
              private guardSev: GuardService,
              @Inject(LOCAL_STORAGE) private storage: WebStorageService) {}

  public setAvailableMenuList() {
    const menuList: any = [];
    let childrenMenuList: any = [];
    this.navigation.forEach((obj: any) => {
      if (obj.menu) {
        if (obj.children) {
          childrenMenuList = [];
          obj.children.forEach((childrenObj: any) => {
            childrenMenuList.push(childrenObj);
          });
          if (childrenMenuList.length > 0) {
            obj.children = childrenMenuList;
            menuList.push(obj);
          }
        } else {
          menuList.push(obj);
        }
      } else {
        menuList.push(obj);
      }
    });

    this.gVariable.navigationMenu = menuList;
  }

}
