import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ModalModule } from 'ngx-bootstrap/modal';
import { FormsModule } from '@angular/forms';

import { ReportMgtRoutingModule } from './report-mgt-routing.module';
import { ReportMgtComponent } from './report-mgt.component';
import { PipeModule, DataGridModule} from './../../core';
import {Rep01Component} from './rep-01/rep01.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    PipeModule,
    DataGridModule,
    ModalModule.forRoot(),
    TabsModule.forRoot(),
    ReportMgtRoutingModule
  ],
  declarations: [
    ReportMgtComponent,
    Rep01Component
  ],
  entryComponents: []
})
export class ReportMgtModule { }
