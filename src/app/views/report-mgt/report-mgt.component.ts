import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';

import {GlobalService} from './../../core';

@Component({
  selector: 'app-manage-config',
  templateUrl: './report-mgt.component.html',
  styleUrls: ['./report-mgt.component.css']
})
export class ReportMgtComponent implements OnInit {

  private mainPath = null;
  private subPath = '';

  private tabList: any[] = [];

  public tabs: any[] = [];

  constructor(private router: Router, private gSev: GlobalService, private activatedRoute: ActivatedRoute) {
    this.activatedRoute.data.subscribe((data: any) => {
      // console.log(data);
      switch (data.key) {
        case 'REP01': {
          this.rep01Tabs();
          this.initRoute();
          break;
        }
        default: {
          break;
        }
      }
    });
  }

  ngOnInit() {}

  onSelectTab(tab: any) {
    tab.active = true;
    this.router.navigate([tab.route]);
  }

  private initRoute() {
    this.tabList.forEach((obj: any) => {
      // if (this.gSev.getAvailableEntitlement(obj.entitlements).AT_LEAST_ONE) {
      if (true) {
        this.tabs.push(obj);
      }
    });
    if (this.tabs.length > 0) {
      this.subPath = this.tabs[0].route;
    }

    this.router.events
      .subscribe((event) => {
        if (event instanceof NavigationEnd) {
          // console.log('NavigationEnd:', event);
          if (event.url === this.mainPath) {
            this.router.navigate([this.subPath]);
          }
          this.activeTab();
        }
      });
    // console.log(this.router);
    if (this.router.url === this.mainPath) {
      this.router.navigate([this.subPath]);
    } else {
      this.activeTab();
    }
  }

  private activeTab() {
    for (const key in this.tabs) {
      if (this.router.url === this.tabs[key].route) {
        this.tabs[key].active = true;
        // return true;
      } else {
        this.tabs[key].active = false;
      }
    }
  }

  private rep01Tabs() {
    this.mainPath = '/rep-01/view';
    this.tabList = [
      {
        title: 'rep01',
        entitlements: [],
        active: false,
        disabled: false,
        route: '/rep-01/view/list'
      }
    ];
  }
}
