import { Component, OnInit } from '@angular/core';

import {AdminService} from '../../../services';
import {StaticConfig} from '../../../core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-domain-master-config',
  templateUrl: './rep01.component.html',
  styleUrls: ['./rep01.component.css']
})
export class Rep01Component implements OnInit {

  public gridOnChangeTime = new Date().getTime();
  public gridEvent: any = {};
  public gridConfig: any = {};
  private recordList: any = [];

  public domainSelected: any = null;
  public domainList: any = [];

  public staticConfig = StaticConfig;

  constructor(
    private adminSev: AdminService,
    private router: Router) {

  }

  ngOnInit() {
    this.initGridConfig();
    this.initDataGrid();
  }

  onGridActionEvent($event) {
    // console.log($event);
    switch ($event.action) {
      default: {
        break;
      }
    }
  }

  private initDataGrid() {
    this.initGridConfig();
    this.gridConfig.apiSev = this.adminSev;
    this.gridConfig.searchParameters = [];
    this.onCallGridEven('refresh', {});
  }

  private onCallGridEven(event: string, data: any) {
    this.gridOnChangeTime = new Date().getTime();
    this.gridEvent.event = event;
    this.gridEvent.data = data;
    // console.log(this.gridEvent);
  }

  private initGridConfig() {
    const statusFilterOptions = [
      { key: StaticConfig.STATUS_LIST.CREATED.ID, value: StaticConfig.STATUS_LIST.CREATED.NAME },
      { key: StaticConfig.STATUS_LIST.PENDING.ID, value: StaticConfig.STATUS_LIST.PENDING.NAME },
      { key: StaticConfig.STATUS_LIST.APPROVED.ID, value: StaticConfig.STATUS_LIST.APPROVED.NAME },
      { key: StaticConfig.STATUS_LIST.SUSPENDED.ID, value: StaticConfig.STATUS_LIST.SUSPENDED.NAME },
      { key: StaticConfig.STATUS_LIST.DELETED.ID, value: StaticConfig.STATUS_LIST.DELETED.NAME }
    ];
    const statusDisplayCondition = [];

    for (const key in StaticConfig.STATUS_LIST || {}) {
      if (StaticConfig.STATUS_LIST.hasOwnProperty(key)) {
        statusDisplayCondition.push({
          key: StaticConfig.STATUS_LIST[key].ID,
          value: StaticConfig.STATUS_LIST[key].NAME,
          style: { color: StaticConfig.STATUS_LIST[key].COLOR}
        });
      }
    }

    this.gridConfig = {
      'apiSev': null,
      'sevFunction': 'shopAllFindByCriteria',
      'apiParameters': {
        'statuses' : [
          StaticConfig.STATUS_LIST.CREATED.ID,
          StaticConfig.STATUS_LIST.PENDING.ID,
          StaticConfig.STATUS_LIST.APPROVED.ID,
          StaticConfig.STATUS_LIST.SUSPENDED.ID,
          StaticConfig.STATUS_LIST.DELETED.ID
        ]
      },
      'searchParameters': [],
      'primaryKey': 'shopId',
      'pagination' : {
        'maxSize' : 5,
        'itemsPerPage' : 15
      },
      'waitingHttpSve' : false,
      'columns': [],
      'records': []
    };

    this.gridConfig.columns.push({
      'name': '',
      'key': 'checkbox',
      'columnType': 'checkbox',
      'headerClass': 'text-center',
      'headerStyle': {},
      'dataStyle': {'text-align': 'center'},
      'width': 30
    });
    this.gridConfig.columns.push({
      'name': 'ID',
      'key': 'shopId',
      'columnType': 'data',
      'headerClass': 'text-left',
      'dataStyle': {'text-align': 'left'},
      'width': 100
    });
    this.gridConfig.columns.push({
      'name': 'Name',
      'key': 'shopName',
      'sort': true,
      'filter': true,
      'filterConfig': {
        'operators': {
          'like': true
        },
        'defaultOperator': 'like'
      },
    });
    this.gridConfig.columns.push({
      'name': 'Email',
      'key': 'email'
    });
    this.gridConfig.columns.push({
      'name': 'City',
      'key': 'city'
    });
    this.gridConfig.columns.push({
      'name': 'Telephone',
      'key': 'telephone'
    });
    this.gridConfig.columns.push({
      'name': 'Status',
      'key': 'status',
      'headerClass': 'text-center',
      'filter': true,
      'filterConfig': {
        'operators': {
          'eq': true
        },
        'defaultOperator': 'eq',
        'type': 'option',
        'options': statusFilterOptions
      },
      'dataDisplayCondition': statusDisplayCondition,
      'dataType': 'number',
      'dataStyle': {'text-align': 'center', 'font-weight': 600},
      'width': 150
    });
    this.gridConfig.columns.push({
      'columnType': 'button',
      'buttonConfig': {
        'action': 'view',
        'name': 'View',
        'class': 'btn-primary',
        'icon': 'fas fa-eye'
      },
      'width': 35
    });
  }

}
