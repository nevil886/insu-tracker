import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReportMgtComponent } from './report-mgt.component';
import {Rep01Component} from './rep-01/rep01.component';

const routes: Routes = [
  {
    path: '',
    component: ReportMgtComponent,
    data: {
      key: 'REP01'
    },
    children: [
      { path: 'list', component: Rep01Component }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportMgtRoutingModule { }
