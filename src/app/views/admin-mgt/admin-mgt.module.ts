import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ModalModule } from 'ngx-bootstrap/modal';
import { FormsModule } from '@angular/forms';
import { ButtonsModule } from 'ngx-bootstrap/buttons';

import { AdminMgtRoutingModule } from './admin-mgt-routing.module';
import { AdminMgtComponent } from './admin-mgt.component';
import { CustomerListComponent } from './customer-list/customer-list.component';
import { CustomerModalComponent } from './customer-modal/customer-modal.component';
import { PipeModule, DataGridModule} from './../../core';
import {CustomerDrivingInsightModalComponent} from './customer-driving-insight-modal/customer-driving-insight-modal.component';
import {GoogleChartsModule} from 'angular-google-charts';
import {AccidentSpotsComponent} from './accident-spots/accident-spots.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    PipeModule,
    DataGridModule,
    ButtonsModule.forRoot(),
    ModalModule.forRoot(),
    TabsModule.forRoot(),
    AdminMgtRoutingModule,
    GoogleChartsModule
  ],
  declarations: [
    CustomerListComponent,
    AccidentSpotsComponent,
    CustomerModalComponent,
    CustomerDrivingInsightModalComponent,
    AdminMgtComponent
  ],
  entryComponents: [
    CustomerModalComponent,
    CustomerDrivingInsightModalComponent
  ]
})
export class AdminMgtModule { }
