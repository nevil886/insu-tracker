import { Component, OnInit } from '@angular/core';
// import {BsModalRef} from 'ngx-bootstrap/modal';
import {Subject} from 'rxjs';
import { BsModalRef } from 'ngx-bootstrap/modal';

import {GlobalVariable} from '../../../core';

@Component({
  selector: 'app-customer-driving-insight-modal',
  templateUrl: './customer-driving-insight-modal.component.html',
  styleUrls: ['./customer-driving-insight-modal.component.css']
})
export class CustomerDrivingInsightModalComponent implements OnInit {
  msgBoxCofRef: BsModalRef;

  public action: string;
  public data: any = {};
  public onClose: Subject<boolean>;

  radioModel = 'M';

  public speedChart: any = {};
  public mileageChart: any = {};

  constructor(public bsModalRef: BsModalRef,
              private gVariable: GlobalVariable) {
    this.onClose = new Subject();
  }

  ngOnInit() {
    this.speedChart.data = [];
    this.mileageChart.data = [];

    setTimeout(() => {
      // console.log(this.data);
      this.speedChart.data = [
        [7.0, -0.2],
        [6.9, 0.8],
        [9.5,  5.7],
        [14.5, 11.3],
        [18.2, 17.0],
        [21.5, 22.0],
        [25.2, 24.8],
        [26.5, 24.1],
        [23.3, 20.1],
        [30.3, 14.1],
        [32.9,  8.6],
        [39.6,  2.5]
      ];
      this.mileageChart.data = [
        [1, -0.2],
        [2, 0.8],
        [3,  5.7],
        [4, 11.3],
        [5, 17.0],
        [6, 22.0],
        [7, 24.8],
        [8, 24.1],
        [9, 20.1],
        [10, 14.1],
        [11,  8.6],
        [12,  2.5]
      ];
    }, 0);
  }

  onCloseModal() {
    const response: any = {};
    this.onClose.next(response);
    this.bsModalRef.hide();
  }

}
