import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';

import {GlobalService} from './../../core';

@Component({
  selector: 'app-manage-config',
  templateUrl: './admin-mgt.component.html',
  styleUrls: ['./admin-mgt.component.css']
})
export class AdminMgtComponent implements OnInit {

  private mainPath = null;
  private subPath = '';

  private tabList: any[] = [];

  public tabs: any[] = [];

  constructor(private router: Router, private gSev: GlobalService, private activatedRoute: ActivatedRoute) {
    this.activatedRoute.data.subscribe((data: any) => {
      // console.log(data);
      switch (data.key) {
        case 'CUSTOMERS': {
          this.customerTabs();
          this.initRoute();
          break;
        }
        case 'DISCOUNT_SUMMARY': {
          this.discountSummaryTabs();
          this.initRoute();
          break;
        }
        case 'ACCIDENT_SPOTS': {
          this.accidentSpotsTabs();
          this.initRoute();
          break;
        }
        default: {
          break;
        }
      }
    });
  }

  ngOnInit() {}

  onSelectTab(tab: any) {
    tab.active = true;
    this.router.navigate([tab.route]);
  }

  private initRoute() {
    this.tabList.forEach((obj: any) => {
      // if (this.gSev.getAvailableEntitlement(obj.entitlements).AT_LEAST_ONE) {
      if (true) {
        this.tabs.push(obj);
      }
    });
    if (this.tabs.length > 0) {
      this.subPath = this.tabs[0].route;
    }

    this.router.events
      .subscribe((event) => {
        if (event instanceof NavigationEnd) {
          // console.log('NavigationEnd:', event);
          if (event.url === this.mainPath) {
            this.router.navigate([this.subPath]);
          }
          this.activeTab();
        }
      });
    // console.log(this.router);
    if (this.router.url === this.mainPath) {
      this.router.navigate([this.subPath]);
    } else {
      this.activeTab();
    }
  }

  private activeTab() {
    for (const key in this.tabs) {
      if (this.router.url === this.tabs[key].route) {
        this.tabs[key].active = true;
        // return true;
      } else {
        this.tabs[key].active = false;
      }
    }
  }

  private customerTabs() {
    this.mainPath = '/admin-mgt/customer';
    this.tabList = [
      {
        title: 'Customers',
        entitlements: [],
        active: false,
        disabled: false,
        route: '/admin-mgt/customer/list'
      }
    ];
  }

  private discountSummaryTabs() {
    this.mainPath = '/admin-mgt/discount-summary';
    this.tabList = [
      {
        title: 'Discount Summary',
        entitlements: [],
        active: false,
        disabled: false,
        route: '/admin-mgt/discount-summary/list'
      }
    ];
  }

  private accidentSpotsTabs() {
    this.mainPath = '/admin-mgt/config/accident-spots';
    this.tabList = [
      {
        title: 'Config Accident Spots',
        entitlements: [],
        active: false,
        disabled: false,
        route: '/admin-mgt/config/accident-spots/list'
      }
    ];
  }
}
