import { Component, OnInit } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { AdminService } from '../../../services';
import {StaticConfig} from '../../../core';
import {Router} from '@angular/router';
import {CustomerModalComponent} from '../customer-modal/customer-modal.component';
import {CustomerDrivingInsightModalComponent} from '../customer-driving-insight-modal/customer-driving-insight-modal.component';

@Component({
  selector: 'app-customer-list',
  templateUrl: './customer-list.component.html',
  styleUrls: ['./customer-list.component.css']
})
export class CustomerListComponent implements OnInit {

  bsModalRef: BsModalRef;

  public gridOnChangeTime = new Date().getTime();
  public gridEvent: any = {};
  public gridConfig: any = {};
  private recordList: any = [];

  public staticConfig = StaticConfig;
  private action = 'view';

  constructor(
    private adminSev: AdminService,
    private modalService: BsModalService,
    private router: Router) {

  }

  ngOnInit() {
    this.initGridConfig();
    // this.initDataGrid();
  }

  onClickView() {
    this.openCustomerModel({});
  }

  onClickViewDrivingInsights() {
    this.openCustomerDrivingInsightModel({});
  }

  onUpdateStatus(statusDetails, status) {
    console.log(statusDetails, status);
    // for (let i = 0; i < this.recordList.length; i++) {
    //   if (this.recordList[i].check) {
    //     this.updateStatus(this.gridConfig.records[i], statusDetails.ID);
    //   }
    // }
  }

  onGridActionEvent($event) {
    // console.log($event);
    switch ($event.action) {
      case 'view': {
        this.action = 'view';
        this.openCustomerModel($event.record || {});
        break;
      }
      case 'check': {
        this.recordList = $event.record || [];
        break;
      }
      default: {
        break;
      }
    }
  }

  private initDataGrid() {
    this.initGridConfig();
    this.gridConfig.apiSev = this.adminSev;
    this.gridConfig.searchParameters = [];
    this.onCallGridEven('refresh', {});
  }

  private openCustomerModel(data: any) {
    const modelConfig: any = {
      class: 'modal-lg',
      animated: true,
      keyboard: true,
      backdrop: true,
      ignoreBackdropClick: true
    };
    this.bsModalRef = null;
    this.bsModalRef = this.modalService.show(CustomerModalComponent, modelConfig);
    this.bsModalRef.content.action = this.action;
    this.bsModalRef.content.data = data;
    this.bsModalRef.content.onClose.subscribe(result => {
      // const response = result;
      // console.log(response);
    });
  }

  private openCustomerDrivingInsightModel(data: any) {
    const modelConfig: any = {
      class: 'modal-lg',
      animated: true,
      keyboard: true,
      backdrop: true,
      ignoreBackdropClick: true
    };
    this.bsModalRef = null;
    this.bsModalRef = this.modalService.show(CustomerDrivingInsightModalComponent, modelConfig);
    this.bsModalRef.content.action = this.action;
    this.bsModalRef.content.data = data;
    this.bsModalRef.content.onClose.subscribe(result => {
      // const response = result;
      // console.log(response);
    });
  }

  private onCallGridEven(event: string, data: any) {
    this.gridOnChangeTime = new Date().getTime();
    this.gridEvent.event = event;
    this.gridEvent.data = data;
    // console.log(this.gridEvent);
  }

  private initGridConfig() {
    const statusFilterOptions = [
      { key: StaticConfig.STATUS_LIST.CREATED.ID, value: StaticConfig.STATUS_LIST.CREATED.NAME },
      { key: StaticConfig.STATUS_LIST.PENDING.ID, value: StaticConfig.STATUS_LIST.PENDING.NAME },
      { key: StaticConfig.STATUS_LIST.APPROVED.ID, value: StaticConfig.STATUS_LIST.APPROVED.NAME },
      { key: StaticConfig.STATUS_LIST.SUSPENDED.ID, value: StaticConfig.STATUS_LIST.SUSPENDED.NAME },
      { key: StaticConfig.STATUS_LIST.DELETED.ID, value: StaticConfig.STATUS_LIST.DELETED.NAME }
    ];
    const statusDisplayCondition = [];
    for (const key in StaticConfig.STATUS_LIST || {}) {
      if (StaticConfig.STATUS_LIST.hasOwnProperty(key)) {
        statusDisplayCondition.push({
          key: StaticConfig.STATUS_LIST[key].ID,
          value: StaticConfig.STATUS_LIST[key].NAME,
          style: { color: StaticConfig.STATUS_LIST[key].COLOR}
        });
      }
    }

    this.gridConfig = {
      'apiSev': null,
      'sevFunction': 'shopAllFindByCriteria',
      'apiParameters': {
        'statuses' : [
          StaticConfig.STATUS_LIST.CREATED.ID,
          StaticConfig.STATUS_LIST.PENDING.ID,
          StaticConfig.STATUS_LIST.APPROVED.ID,
          StaticConfig.STATUS_LIST.SUSPENDED.ID,
          StaticConfig.STATUS_LIST.DELETED.ID
        ]
      },
      'searchParameters': [],
      'primaryKey': 'shopId',
      'pagination' : {
        'maxSize' : 5,
        'itemsPerPage' : 15
      },
      'waitingHttpSve' : false,
      'columns': [],
      'records': []
    };

    this.gridConfig.columns.push({
      'name': '',
      'key': 'checkbox',
      'columnType': 'checkbox',
      'headerClass': 'text-center',
      'headerStyle': {},
      'dataStyle': {'text-align': 'center'},
      'width': 30
    });
    this.gridConfig.columns.push({
      'name': 'ID',
      'key': 'shopId',
      'columnType': 'data',
      'headerClass': 'text-left',
      'dataStyle': {'text-align': 'left'},
      'width': 100
    });
    this.gridConfig.columns.push({
      'name': 'Name',
      'key': 'shopName',
      'sort': true,
      'filter': true,
      'filterConfig': {
        'operators': {
          'like': true
        },
        'defaultOperator': 'like'
      },
    });
    this.gridConfig.columns.push({
      'name': 'Contact No',
      'key': 'email'
    });
    this.gridConfig.columns.push({
      'name': 'Insu Policy No',
      'key': 'city'
    });
    this.gridConfig.columns.push({
      'name': 'Vehicle Reg No',
      'key': 'telephone'
    });
    this.gridConfig.columns.push({
      'name': 'Status',
      'key': 'status',
      'headerClass': 'text-center',
      'filter': true,
      'filterConfig': {
        'operators': {
          'eq': true
        },
        'defaultOperator': 'eq',
        'type': 'option',
        'options': statusFilterOptions
      },
      'dataDisplayCondition': statusDisplayCondition,
      'dataType': 'number',
      'dataStyle': {'text-align': 'center', 'font-weight': 600},
      'width': 150
    });
    this.gridConfig.columns.push({
      'columnType': 'button',
      'buttonConfig': {
        'action': 'view',
        'name': 'View',
        'class': 'btn-primary',
        'icon': 'fas fa-eye'
      },
      'width': 35
    });
  }

  private updateStatus (record, statusId) {
    const req = {
      'primaryId': record.shopId,
      'status': statusId
    };

    // this.adminSev.updateShopStatus(req).then((response: any) => {
    //   // console.log(response);
    //   const data = {
    //     'shopId': response.id,
    //     'status': statusId
    //   };
    //   this.onCallGridEven('edit', data);
    // }).catch((error: any) => {});
  }

}
