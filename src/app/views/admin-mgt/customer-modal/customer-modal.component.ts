import { Component, OnInit } from '@angular/core';
// import {BsModalRef} from 'ngx-bootstrap/modal';
import {Subject} from 'rxjs';
import {NgForm} from '@angular/forms';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import {GlobalVariable, ToastService} from '../../../core';

@Component({
  selector: 'app-customer-modal',
  templateUrl: './customer-modal.component.html',
  styleUrls: ['./customer-modal.component.css']
})
export class CustomerModalComponent implements OnInit {
  msgBoxCofRef: BsModalRef;

  public action: string;
  public data: any = {};
  public onClose: Subject<boolean>;
  public formData: any = {};

  constructor(public bsModalRef: BsModalRef,
              private toastNot: ToastService,
              private gVariable: GlobalVariable) {
    this.onClose = new Subject();
  }

  ngOnInit() {
    setTimeout(() => {
      console.log(this.data);
    }, 0);
  }

  onCloseModal() {
    const response: any = {};
    this.onClose.next(response);
    this.bsModalRef.hide();
  }

  onSubmit(form: NgForm) {
    if (form.valid) {
      try {
        const value = JSON.parse(form.value.value);
        // console.log(req);
      } catch (e) {
        console.log(e);
      }
    }
  }

}
