import { Component, OnInit } from '@angular/core';

import { AdminService } from '../../../services';
import {Router} from '@angular/router';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-accident-spots',
  templateUrl: './accident-spots.component.html',
  styleUrls: ['./accident-spots.component.css']
})
export class AccidentSpotsComponent implements OnInit {

  public formData: any = {};

  constructor(
    private adminSev: AdminService, private router: Router) {

  }

  ngOnInit() {

  }

  onSubmit(form: NgForm) {
    if (form.valid) {
      try {
        console.log(form.value);
        // const value = JSON.parse(form.value.value);
        // console.log(req);
      } catch (e) {
        console.log(e);
      }
    }
  }

}
