import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CustomerListComponent } from './customer-list/customer-list.component';
import { AdminMgtComponent } from './admin-mgt.component';
import {AccidentSpotsComponent} from './accident-spots/accident-spots.component';

const routes: Routes = [
  {
    path: 'customer',
    component: AdminMgtComponent,
    data: {
      key: 'CUSTOMERS'
    },
    children: [
      { path: 'list', component: CustomerListComponent }
    ]
  },
  // {
  //   path: 'discount-summary',
  //   component: AdminMgtComponent,
  //   data: {
  //     key: 'DISCOUNT_SUMMARY'
  //   },
  //   children: [
  //     { path: 'list', component: CustomerListComponent }
  //   ]
  // },
  {
    path: 'config/accident-spots',
    component: AdminMgtComponent,
    data: {
      key: 'ACCIDENT_SPOTS'
    },
    children: [
      { path: 'list', component: AccidentSpotsComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminMgtRoutingModule { }
