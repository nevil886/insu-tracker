import {Injectable} from '@angular/core';
import {Md5} from 'ts-md5/dist/md5';

import {ApiServiceConfig, GlobalVariable, HttpService} from '../core';

@Injectable()
export class AdminService {

  constructor(private httpService: HttpService, private gVariable: GlobalVariable) { }

  public login(req) {
    const formattedReq = {
      'loginName': req.loginName || null,
      'password':  Md5.hashStr(req.password || '') || null
    };

    return new Promise((resolve, reject) =>
      this.httpService.httpPut(ApiServiceConfig.ADMIN_API_SERVICE, '/login', formattedReq, {})
        .then((data: any) => {
          if (data) {
            resolve(data);
          } else {
            reject('no content 204');
          }
        }).catch((error: any) => {
        reject(error);
      }));
  }

}
